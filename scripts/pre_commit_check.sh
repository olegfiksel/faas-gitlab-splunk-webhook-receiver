#!/bin/bash

echo "gofmt"
gofmt -l *.go

echo "golint"
if [[ -z $(which golint) ]]; then
    echo "golint not found, installing..."
    cur_dir=${PWD}
    cd /tmp && go get -u golang.org/x/lint/golint && cd "$cur_dir"
    echo "Done!"
fi
golint -set_exit_status

echo "staticcheck"
# https://staticcheck.io/docs
if [[ -z $(which staticcheck) ]]; then
    echo "staticcheck not found, installing..."
    cur_dir=${PWD}
    cd /tmp && go get honnef.co/go/tools/cmd/staticcheck && cd "$cur_dir"
    echo "Done!"
fi
staticcheck

echo "gosec"
if [[ -z $(which gosec) ]]; then
    echo "gosec not found, installing..."
    cur_dir=${PWD}
    cd /tmp && go get github.com/securego/gosec/cmd/gosec && cd "$cur_dir"
    echo "Done!"
fi
gosec -exclude=G402 .