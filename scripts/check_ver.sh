#!/bin/sh

VER=$(grep image: faas-gitlab-splunk-webhook-receiver.yml|sed -re 's/.*://')
if [ "${CI_COMMIT_REF_NAME}" != "${VER}" ]; then
    echo "Docker image tag in faas-gitlab-splunk-webhook-receiver.yml (${VER}) different than the git tag name (${CI_COMMIT_REF_NAME})"
    echo "Correct the image tag in faas-gitlab-splunk-webhook-receiver.yml"
    exit 1
fi
echo "Versions are equal (${VER} = ${CI_COMMIT_REF_NAME}), proceeding..."
