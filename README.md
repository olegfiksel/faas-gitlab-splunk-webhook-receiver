[![Go Report Card](https://goreportcard.com/badge/gitlab.com/olegfiksel/faas-gitlab-splunk-webhook-receiver/badge/gitlab.com/olegfiksel/faas-gitlab-splunk-webhook-receiver)](https://goreportcard.com/report/gitlab.com/olegfiksel/faas-gitlab-splunk-webhook-receiver/badge/gitlab.com/olegfiksel/faas-gitlab-splunk-webhook-receiver)  [![codecov](https://codecov.io/gl/olegfiksel/faas-gitlab-splunk-webhook-receiver/branch/master/graph/badge.svg)](https://codecov.io/gl/olegfiksel/faas-gitlab-splunk-webhook-receiver) [![Coverage Status](https://coveralls.io/repos/gitlab/olegfiksel/faas-gitlab-splunk-webhook-receiver/badge.svg?branch=master)](https://coveralls.io/gitlab/olegfiksel/faas-gitlab-splunk-webhook-receiver?branch=master) [![Matrix Channel](https://img.shields.io/matrix/faas-gitlab-splunk-webhook-receiver:fiksel.info.svg?label=%23faas-gitlab-splunk-webhook-receiver%3Afiksel.info&logo=matrix&server_fqdn=matrix.fiksel.info)](https://matrix.to/#/#faas-gitlab-splunk-webhook-receiver:fiksel.info)

# OpenFaaS function to receive webhooks from GitLab and send the data to Splunk using HEC (Http-Event-Collector)

# Architecture

![Arch diagram](/images/arch_diagram.jpg )

# Configuration

## GitLab Webhook

* Head to https://gitlab-server/user/repo/hooks
* Create new webhook by specifying:
  * URL: `http://openfaas-gateway:8080/function/gitlab-splunk-webhook-receiver`
  * Secret Token: `blah-blah-blah-long-shared-token`
* Check the checkboxes you want to get data on (Push events, Pipelines, Jobs, etc)
* Click "Add webhook"
* There is also a "test" button on the created webhook that can be used later for firing test events

## FaaS GitLab authentication

This is used to authenticate GitLab Webhook.

* Secret `gitlab-token` = `blah-blah-blah-long-shared-token` [Details](https://docs.gitlab.com/ce/user/project/integrations/webhooks.html#secret-token)

```
faas-cli secret create gitlab-token --gateway http://openfaas-gateway:8080 --from-literal='blah-blah-blah-long-shared-token'
```

## FaaS Splunk authentication

This is used to authenticate the data injestion in Splunk.

Splunk HEC (HTTP Event Collector) needs to be created and enable in Splunk. See [this howto](https://docs.splunk.com/Documentation/Splunk/latest/Data/UsetheHTTPEventCollector) for details on this.

* Secret `splunk-url` = `https://splunk:8088/services/collector/raw?channel=48211655-8036-44be-adf8-3f312af0ab7f&index=splunk_index`
* Secret `splunk-token` = `Splunk 48211655-8036-44be-adf8-3f312af0ab7f` [Details](https://docs.splunk.com/Documentation/Splunk/8.0.4/Data/HECExamples)

```
faas-cli secret create splunk-url [--gateway http://openfaas-gateway:8080] --from-literal='https://splunk:8088/services/collector/raw?channel=48211655-8036-44be-adf8-3f312af0ab7f&index=gitlab_on-prem'

faas-cli secret create splunk-token [--gateway http://openfaas-gateway:8080] --from-literal='Splunk 48211655-8036-44be-adf8-3f312af0ab7f'
```

# Installation

* Download `faas-gitlab-splunk-webhook-receiver.yml` from [releases](https://gitlab.com/olegfiksel/faas-gitlab-splunk-webhook-receiver/-/releases)
* Deploy using faas-cli
  * `faas-cli deploy -f ./faas-gitlab-splunk-webhook-receiver.yml [--gateway http://http://openfaas-gateway:8080] --read-template=false`
    * Tip: you can set environment variable `OPENFAAS_URL` and avoid using `--gateway` in every command

# Building yourself (advanced)

## Installation

* Clone the repo
  * `git clone https://gitlab.com/olegfiksel/faas-gitlab-splunk-webhook-receiver.git && cd faas-gitlab-splunk-webhook-receiver`
* Download golang-http template
  * `faas-cli template pull https://github.com/openfaas-incubator/golang-http-template`
* Deploy the function
  * `faas-cli deploy [--gateway http://openfaas-gateway:8080] -f faas-gitlab-splunk-webhook-receiver.yml`
  * Tip: you can set environment variable `OPENFAAS_URL` and avoid using `--gateway` in every command

## Deployment

```
faas-cli build -f ./faas-gitlab-splunk-webhook-receiver.yml [--gateway=http://yourgateway:8080]
faas-cli deploy -f ./faas-gitlab-splunk-webhook-receiver.yml [--gateway=http://yourgateway:8080]
```

# Known bugs

* If you are using [scale-to-zero feature (faas-idler)](https://github.com/openfaas-incubator/faas-idler) and `/async-function/` the webhook is received with HTTP 202 and the function counter is incremented but the function is not executed and also no data is injested in Splunk.

