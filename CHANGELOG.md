# v1.0.1

## Bugfixes

* Remove splunk token printing in the log (#7)

# v1.0.0

First release 🎉
