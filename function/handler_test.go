package function

import (
	//"github.com/openfaas-incubator/go-function-sdk"
	"testing"
)

func Test_IsAuthorized(t *testing.T) {
	var tests = []struct {
		description        string
		requestGitlabToken string
		body               string
		gitlabToken        string
		debug              bool
		expectedSuccess    bool
	}{
		{
			"Success",
			"blah-blah-gitlab-token",
			"This is a test HTTP body",
			"blah-blah-gitlab-token",
			true,
			true,
		},
		{
			"Token mismatch",
			"blah-blah-gitlab-token",
			"This is a test HTTP body",
			"completely-different-token",
			true,
			false,
		},
		{
			"Missing token in the request",
			"",
			"This is a test HTTP body",
			"blah-blah-gitlab-token",
			true,
			false,
		},
		{
			"Missing body",
			"blah-blah-gitlab-token",
			"",
			"blah-blah-gitlab-token",
			true,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			ll := IsAuthorized(tt.requestGitlabToken, tt.body, tt.gitlabToken, tt.debug)
			if ll != tt.expectedSuccess {
				t.Errorf("IsAuthorized returned %+v, want %+v", ll, tt.expectedSuccess)
			}
		})
	}
}
