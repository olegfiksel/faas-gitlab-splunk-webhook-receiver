package function

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"github.com/openfaas-incubator/go-function-sdk"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

// Interface for mocking the file read
var fileReader = ioutil.ReadFile

// FakeReadFiler is used as a faked filesystem
type FakeReadFiler struct {
	Str string
}

// ReadFile is used to mock fake file
func (f FakeReadFiler) ReadFile(filename string) ([]byte, error) {
	buf := bytes.NewBufferString(f.Str)
	return ioutil.ReadAll(buf)
}

// ReadSecret reads a secret from the file system
func ReadSecret(envVarName string) string {
	file := os.Getenv(envVarName)
	secret, err := fileReader("/var/openfaas/secrets/" + file)
	if err != nil {
		log.Fatal(err)
	}
	return string(secret)
}

// IsAuthorized checks the HTTP header sent by the GitLab
// against the shared secret provided to this function on startup
func IsAuthorized(requestGitlabToken string, body string, gitlabToken string, debug bool) bool {
	if debug {
		fmt.Fprintf(os.Stderr, "X-Gitlab-Token: %s\n", body)
		fmt.Fprintf(os.Stderr, "gitlab_token_secret: %s\n", requestGitlabToken)
	}
	if requestGitlabToken != string(gitlabToken) {
		return false
	}
	return true
}

// Handle handles the http request
func Handle(req handler.Request) (handler.Response, error) {
	debug := false
	if os.Getenv("DEBUG") == "true" {
		debug = true
	}
	insecureSkipVerify := false
	if os.Getenv("insecure_skip_verify") == "true" {
		insecureSkipVerify = true
	}

	// Read secrets
	gitlabToken := ReadSecret("gitlab_token_secret")
	splunkURL := ReadSecret("splunk_url_secret")
	splunkToken := ReadSecret("splunk_token_secret")

	// Authorize GitLab
	if !IsAuthorized(string(req.Header.Get("X-Gitlab-Token")), string(req.Body), gitlabToken, debug) {
		return handler.Response{
			Body:       []byte("Invalid token"),
			StatusCode: http.StatusUnauthorized,
		}, nil
	}

	// Send request to Splunk
	transportConfig := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: insecureSkipVerify},
	}
	client := &http.Client{Transport: transportConfig}
	req2, err := http.NewRequest("POST", splunkURL, bytes.NewBuffer(req.Body))
	if err != nil {
		panic(err)
	}
	req2.Header.Set("Authorization", splunkToken)
	resp, err := client.Do(req2)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
	return handler.Response{
		Body:       []byte(string(req.Body)),
		StatusCode: http.StatusOK,
	}, nil
}
